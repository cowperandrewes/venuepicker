#pylint: disable=missing-docstring

import json as sys_json

class DummyResponse():
    def __init__(self, *args, **kwargs):
        self.venue_content = """
            [
                {
                "name": "El Cantina",
                "food": ["Mexican"],
                "drinks": ["Soft drinks", "Tequila", "Beer"]
                },
                {
                "name": "Twin Dynasty",
                "food": ["Chinese"],
                "drinks": ["Soft Drinks", "Rum", "Beer", "Whisky", "Cider"]
                },
                {
                "name": "Spice of life",
                "food": ["Eggs", "Meat", "Fish", "Pasta", "Dairy"],
                "drinks": ["Vokda", "Gin", "whisky", "Rum", "Cider", "Beer", "Soft drinks"]
                },
                {
                "name": "The Cambridge",
                "food": ["Eggs", "Meat", "Fish", "Pasta", "Dairy"],
                "drinks": ["Vokda", "Gin", "Cider", "Beer", "Soft drinks"]
                },
                {
                "name": "Wagamama",
                "food": ["Japanese"],
                "drinks": ["Beer", "Cider", "Soft Drinks", "Sake"]
                },
                {
                "name": "Sultan Sofrasi",
                "food": ["Meat", "Bread", "Fish"],
                "drinks": ["Beer", "Cider", "Soft Drinks"]
                },
                {
                "name": "Spirit House",
                "food": ["Nuts", "Cheese", "Fruit"],
                "drinks": ["Vodka", "Gin", "Rum", "Tequila"]
                },
                {
                "name": "Tally Joe",
                "food": ["Fish", "Meat", "Salad", "Deserts"],
                "drinks": ["Beer", "Cider", "Soft Drinks", "Sake"]
                },
                {
                "name": "Fabrique",
                "food": ["Bread", "Cheese", "Deli"],
                "drinks": ["Soft Drinks", "Tea", "Coffee"]
                }
            ]
        """

        self.user_content = """
            [
                {
                "name": "John Davis",
                "wont_eat": ["Fish"],
                "drinks": ["Cider", "Rum", "Soft drinks"]
                },
                {
                "name": "Gary Jones",
                "wont_eat": ["Eggs", "Pasta"],
                "drinks": ["Tequila", "Soft drinks", "beer", "Tea", "Coffee"]
                },
                {
                "name": "Robert Webb",
                "wont_eat": ["Bread", "Pasta"],
                "drinks": ["Vokda", "Gin", "Whisky", "Rum"]
                },
                {
                "name": "Gavin Coulson",
                "wont_eat": [],
                "drinks": ["Cider", "Beer", "Rum", "Soft drinks"]
                },
                {
                "name": "Alan Allen",
                "wont_eat": ["Meat", "Fish"],
                "drinks": ["Soft drinks", "Tea"]
                },
                {
                "name": "Bobby Robson",
                "wont_eat": ["Mexican"],
                "drinks": ["Vokda", "Gin", "whisky", "Rum", "Cider", "Beer", "Soft drinks"]
                },
                {
                "name": "David Lang",
                "wont_eat": ["Chinese"],
                "drinks": ["Beer", "cider", "Rum"]
                } 
            ]

        """

        if kwargs.get("endpoint", "venues") == "venues":
            self.content = self.venue_content
        else:
            self.content = self.user_content

    def __str__(self):
        return "<DummyResponse Object>"

    def json(self, **kwargs):
        return sys_json.loads(self.content, **kwargs)
    def text(self):
        return self.content

def get_dummy_venue_json():
    return DummyResponse(endpoint="venues").json()
def get_dummy_user_json():
    return DummyResponse(endpoint="users").json()
