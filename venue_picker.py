"""
    Backend implementation for restaurant search functionality.
"""
import typing
import functools
import json
import argparse
import sys
import requests

import dummy_functions
import runtime_vars


class LiveFunction():
    """
        For ease of development, allow this decorator to interpose itself
        and redirect the function call if conditions are met.

        This allows easy dev in offline modes, or in the case we are using
        it here - when sample data endpoints provided for development are
        syntactically incorrect.

        venues.json at https://gist.github.com/benjambles/ea36b76bc5d8ff09a51def54f6ebd0cb
        has a extra ',' on line 46 which prevents successful parsing
    """
    #pylint: disable=too-few-public-methods

    def __init__(self, dev_function: typing.Callable[..., typing.Any]):
        self.dev_f = dev_function

    def __call__(self, func: typing.Callable[..., typing.Any]):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            if runtime_vars.live:
                return func(*args, **kwargs)
            return self.dev_f(*args, **kwargs)
        return wrapped

def get_live_data(url: str):
    """
        Gets data from a live source or raises exception
    """
    try:
        resp = requests.get(url)
        # raise an exception for http errors
        resp.raise_for_status()
        return resp
    except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as http_e:
        print(http_e)
        raise

@LiveFunction(dev_function=dummy_functions.get_dummy_user_json)
def users() -> typing.List[dict]:
    """
        Get all User data, or raise exception
    """
    try:
        return get_live_data(runtime_vars.user_url).json()
    except json.JSONDecodeError as jde:
        raise Exception("Couldn't Decode JSON from Endpoint: {} {}".format(runtime_vars.user_url, jde))
    except Exception as exc:
        raise Exception("Couldnt get data from Endpoint: {} {}".format(runtime_vars.user_url, exc))

@LiveFunction(dev_function=dummy_functions.get_dummy_venue_json)
def venues() -> typing.List[dict]:
    """
        Get all Venue data, or raise exception
    """
    try:
        return get_live_data(runtime_vars.venue_url).json()
    except json.JSONDecodeError as jde:
        raise Exception("Couldn't Decode JSON from Endpoint: {} {}".format(runtime_vars.user_url, jde))
    except Exception as exc:
        raise Exception("Couldnt get data from Endpoint: {} {}".format(runtime_vars.user_url, exc))

def lower_list(tgt_list: typing.List[str]) -> typing.List[str]:
    """
        return a new list, with each of the elements
        converted to lower case.
    """
    return list(map(lambda x: x.lower(), tgt_list))

def users_by_name(user_names: typing.List[dict] = []): #pylint: disable=dangerous-default-value
    """
        Return user records for any user whose name
        appears in user_names
    """

    all_users = users()
    return [x for x in all_users if x["name"] in user_names]

def venues_for_users(party: typing.List[dict] = users(), insensitive: bool = False):
    """
        Find a restaurant which caters for all members of a party

        <snip>
        Sample Output
            Places to go:
                The Diner
                The cambridge
                The spice of life
            Places to avoid:
                Loch Fine
                    Ben doesn't eat fish
                    Far doesn't drink wine
        </snip>

        Loch Fine presumably serves more than just fish and wine
        (vegetarian options, steak, soft-drinks etc) but still ends up
        in the list of venues to avoid in the example output, for one
        employee the reason is the food, for another it appears to be the
        drink..

        Since Loch Fyne is not included in the sample venue data, the above
        cannot assumption cannot be resolved. It is not clear whether the
        example is based on Loch Fine data in which fish and wine are the only
        two food and drink items available. (The alternative is considerably
        bleaker, where employees treat a dislike in the same vein as an allergy
        and will only frequent venues which do not serve anything in their
        dislike list)

        Sample Output also does not show examples of multiple exclusion matches,
        I.e. What if Ben doesn't eat Fish or Meat or Eggs.


        This function therefore implements the following, based on assumptions
        for the above.

        A Restaurant is excluded IFF either of the two following conditions are
        met:

            1 - Once the employee's food dislikes (wont_eat) are taken
            into account, no food options remain.
            2 - The venue does not contain any drink options which match
            any of the employee's drink choices.

        Sample Data has not been sanitised/normalised and an anomaly exists:
            1 - users.json and venues.json use "Soft Drinks" and "Soft drinks"
            interchangeably.

            It was not possible to know, at time of writing, if this is
            expected, or if the source data needs correction.

            Due to the above anomaly, this function operates in two possible
            modes, case sensitive, or insensitive.
    """

    if not party:
        party = users()

    if insensitive:
        print("Performing Insensitive Search")

    all_venues = venues()

    for index, venue in enumerate(all_venues):
        """
            For each Venue,
                check each party member to see if the food and drink
                is compatible.

                If either food or drink is incompatible, annotate the reason
                in a new property within the venue dict.

            Returns two lists:
                1 - A list of venues to visit
                2 - A list of venues to avoid

            We could stop searching through users for incompatibilities after the
            first time a venue is found to be incompatible. This would result
            in fewer operations, but would also only blame the first employee
            instead of all employees with whom the venue is incompatible.
        """ #pylint: disable=pointless-string-statement

        all_venues[index]["reasons"] = []
        for user in party:
            incompat_food = False
            incompat_drinks = False

            # if removing the party members food choices from the venue, leaves
            # no available choices, then the venue is incompatible
            if insensitive:
                if not set(lower_list(venue["food"])) - set(lower_list(user["wont_eat"])):
                    incompat_food = True
            else:
                if not set(venue["food"]) - set(user["wont_eat"]):
                    incompat_food = True

            # if none of the party member's drink choices exist within the
            # venue drinks choices, then the venue is incompatible
            if insensitive:
                if not set(lower_list(venue["drinks"])).intersection(set(lower_list(user["drinks"]))):
                    incompat_drinks = True
            else:
                if not set(venue["drinks"]).intersection(set(user["drinks"])):
                    incompat_drinks = True

            if incompat_food:
                all_venues[index]["reasons"].append("{} doesn't eat {}".format(
                    user["name"],
                    ", ".join(venue["food"])
                    ))

            if incompat_drinks:
                all_venues[index]["reasons"].append("{} doesn't drink {}".format(
                    user["name"],
                    ", ".join(venue["drinks"]),
                    ))

    return([x for x in all_venues if not x["reasons"]], [x for x in all_venues if x["reasons"]])

def print_results(places_to_visit: typing.List[dict] = [], places_to_avoid: typing.List[dict] = []):
    """
        Print, to the example output spec, the information
        regarding places to visit and places to avoid.

        Takes two lists as arguments, each contains Venue
        dictionaries. places_to_avoid should contain Venue
        dictionaries annotated with an extra property "reasons".
    """
    #pylint: disable=dangerous-default-value
    if places_to_visit:
        print("Places to go:")
        for venue in places_to_visit:
            print("\t{}".format(venue["name"]))
        print("")
    else:
        print("No Place to go...")

    if places_to_avoid:
        print("Places to avoid:")
        for venue in places_to_avoid:
            print("\t{}".format(venue["name"]))
            print("\t\t" + "\n\t\t".join(venue["reasons"]))
    else:
        print("Nowhere to avoid!")

def main():
    """
        Main function, called when invoked from the command line
    """
    parser = argparse.ArgumentParser(
        description="Find restaurants which cater for employee preferences")
    parser.add_argument("-g", "--group-members",
                        metavar="employee_name",
                        nargs="+",
                        help="Attendees")
    parser.add_argument("-i", "--insensitive",
                        action="store_true",
                        help="Use case-insensitive matching on strings")
    parser.add_argument("-l", "--list",
                        action="store_true",
                        help="List All Employees")
    args = parser.parse_args()

    if args.list:
        try:
            members = users()
        except Exception as e:
            print(e)
            sys.exit(1)

        for m in members:
            print(m["name"])
    else:
        try:
            members = users()
        except Exception as e:
            print(e)
            sys.exit(1)

        if "error" in members[0].keys():
            print("Error: {}".format(members[0]["error"]))
            sys.exit(1)

        if args.group_members:
            tmp = []
            for m in members:
                if m["name"] in args.group_members:
                    tmp.append(m)
            members = tmp

        try:
            print_results(*venues_for_users(party=members, insensitive=args.insensitive))
        except Exception as e:
            print(e)
            sys.exit(1)

if __name__ == "__main__":
    main()
