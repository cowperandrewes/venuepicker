"""
    Simple Flask function to process the user
    selection form and perform the search.
"""
import flask

# import our backend functions
import venue_picker

APP = flask.Flask(__name__)

@APP.route("/", methods=['GET', 'POST'])
def index():
    """
        Single page for displaying and processing
        form for input.

	Very simple, basic error checking.

	Note that the pylint directive disabling would go away
	with correct exception definition, propagation and
	handling.
    """
    try:
        users = venue_picker.users()
    except Exception as exc: #pylint: disable=broad-except
        return flask.render_template('index.html',
                                     users=[],
                                     error="{}".format(exc))

    if flask.request.method == 'POST':
        if not flask.request.form.getlist('member'):
            # No users were chosen
            return flask.render_template('index.html',
                                         users=users,
                                         error="No Members Chosen")

        try:
            user_names = flask.request.form.getlist('member')
            members = venue_picker.users_by_name(user_names)
            results = {
                'visit':[],
                'avoid':[]
            }
            results['visit'], results['avoid'] = venue_picker.venues_for_users(
                party=members,
                insensitive=True)

            return flask.render_template('index.html',
                                         results=results,
                                         users=users,
                                         chosen=members)

        except Exception as exc: #pylint: disable=broad-except
            return flask.render_template('index.html',
                                         users=[],
                                         error="{}".format(exc))

    return flask.render_template('index.html', users=users)
