================================================================================
README - Venue Picker
================================================================================

The delivered backend implementation runs from both the command line, and Flask.

The example output endpoints supplied in the received documentatation have
the following issues:

	venues.json:
		Extra comma on line 46 prevents this data from parsing
		Mixed use of 'Soft Drinks' and 'Soft drinks' in data requires
			case-insensitive implementation for search to avoid
			false negatives.
			By Default, Flask implementation uses insensitive match.

Further issues and implementation details are described in the source for
venue_picker.py

To work around these data integrity issues, dummy_functions.py implements a fake
response containing corrected copies of the example data.

runtime_vars.py is used to store the following variables:

	venue_url The URL for the live venue data.
	user_url: The URL for the live user data.
	live: Whether or not to use live endpoint data (setting to false causes
		dummy_functions.py to be used).

Dependencies are:
	python3.7+ (static type annotations required)
	Flask (version 1.0.2 used for dev)
	requests (version 2.21.0 used for dev)

** No CSRF/Security/Auth is implemented in this POC.

================================================================================
Pre-requisite Installation
================================================================================
Once you have this workspace, pre-requisites for development can be met using
the following:

	$ virtualenv <path_to_desired_virtualenv>
	$ . <path_to_desired_virtualenv>/bin/activate
	(venv) $ pip install Flask requests

================================================================================
Command Line Invocation
================================================================================
Example cmd-line usage:

	$ python ./venue_picker.py -h
	usage: venue_picker.py [-h] [-g employee_name [employee_name ...]] [-i] [-l]

	Find restaurants which cater for employee preferences

	optional arguments:
	  -h, --help            show this help message and exit
	  -g employee_name [employee_name ...], --group-members employee_name [employee_name ...]
				Attendees
	  -i, --insensitive     Use case-insensitive matching on strings
	  -l, --list            List All Employees

By default, all employees are included in the group

	$ python ./venue_picker.py -i 
	Places to go:
		Spice of life
		The Cambridge

	Places to avoid:
		El Cantina
			Robert Webb doesn't drink Soft drinks, Tequila, Beer
			Bobby Robson doesn't eat Mexican
		Twin Dynasty
			David Lang doesn't eat Chinese
		Wagamama
			Robert Webb doesn't drink Beer, Cider, Soft Drinks, Sake
		Sultan Sofrasi
			Robert Webb doesn't drink Beer, Cider, Soft Drinks
		Spirit House
			Alan Allen doesn't drink Vodka, Gin, Rum, Tequila
		Tally Joe
			Robert Webb doesn't drink Beer, Cider, Soft Drinks, Sake
		Fabrique
			Robert Webb doesn't drink Soft Drinks, Tea, Coffee
			David Lang doesn't drink Soft Drinks, Tea, Coffee


To Narrow down the search parameters, supply them via the -g/--group-members argument:

	$ python ./venue_picker.py \
		-i \
		--group-members "John Davis" "Gary Jones" "Robert Webb" "Bobby Robson"

	Places to go:
		Twin Dynasty
		Spice of life
		The Cambridge
		Spirit House

	Places to avoid:
		El Cantina
			Robert Webb doesn't drink Soft drinks, Tequila, Beer
			Bobby Robson doesn't eat Mexican
		Wagamama
			Robert Webb doesn't drink Beer, Cider, Soft Drinks, Sake
		Sultan Sofrasi
			Robert Webb doesn't drink Beer, Cider, Soft Drinks
		Tally Joe
			Robert Webb doesn't drink Beer, Cider, Soft Drinks, Sake
		Fabrique
			Robert Webb doesn't drink Soft Drinks, Tea, Coffee

For approachability, the employees can be listed using the -l/--list argument:

	$ python ./venue_picker.py --list
	John Davis
	Gary Jones
	Robert Webb
	Gavin Coulson
	Alan Allen
	Bobby Robson
	David Lang


================================================================================
Flask Invocation
================================================================================

To run the development Flask server, from the root directory of the workspace
utter:

	$ FLASK_APP=app.py flask run

This will start the server on localhost:5000

Only one endpoint url exists (/), this endpoint will render a form of employees
to allow a chosen set of group members. The search can then be submitted and
results will be rendered into the reloaded page.
